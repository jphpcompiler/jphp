<?php
namespace php\framework\loader;

use Traversable;

/**
 * Class FileClassLoader
 * @package php\framework
 */
class FileClassLoader extends ClassLoader
{
    /**
     * @param string $prefix
     * @param string|array|Traversable $directories
     */
    public function addPrefix($prefix, $directories)
    {

    }

    /**
     * @param array|Traversable $prefixes
     */
    public function addPrefixes($prefixes)
    {

    }

    /**
     * @param string $className
     */
    public function loadClass($className)
    {

    }

    /**
     * @param string $className
     * @return string
     */
    public function findClass($className)
    {

    }
}